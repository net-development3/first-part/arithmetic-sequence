﻿using System;

namespace ArithmeticSequenceTask
{
    public static class ArithmeticSequence
    {
        public static int Calculate(int number, int add, int count)
        {
            if (count <= 0)
            {
                throw new ArgumentException("The count of elements of the sequence cannot be less or equals zero.", nameof(count));
            }

            long sum = 0;
            for (int i = 0; i < count; i++)
            {
                sum += number;
                if (number >= int.MaxValue || number <= int.MinValue)
                {
                    throw new OverflowException();
                }

                number += add;
            }

            if (sum >= int.MaxValue || sum <= int.MinValue)
            {
                throw new OverflowException();
            }

            return unchecked((int)sum);
        }
    }
}
